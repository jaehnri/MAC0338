package main

import "fmt"

func mergeSort(items []int) ([]int, int) {
	if len(items) < 2 {
		return items, 0
	}
	left, leftInversions := mergeSort(items[:len(items)/2])
	right, rightInversions := mergeSort(items[len(items)/2:])
	merged, mergedInversions := merge(left, right)

	return merged, leftInversions + rightInversions + mergedInversions
}

func merge(left []int, right []int) ([]int, int) {
	var merged []int
	inversions := 0
	i := 0
	j := 0
	for i < len(left) && j < len(right) {
		if left[i] <= right[j] {
			merged = append(merged, left[i])
			i++
		} else {
			// left[i] > right[j] means that left[i + 1], left[i + 2], ... are all greater than right[j]
			inversions += len(left) - i
			merged = append(merged, right[j])
			j++
		}
	}

	// One of the slices is over, so we just append the rest of the remainder one
	for ; i < len(left); i++ {
		merged = append(merged, left[i])
	}
	for ; j < len(right); j++ {
		merged = append(merged, right[j])
	}

	return merged, inversions
}

func main() {
	unsorted := []int{5, 2, 4, 3, 1}
	_, inversions := mergeSort(unsorted)
	fmt.Print(inversions)
}
