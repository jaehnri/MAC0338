package main

import (
	"fmt"
	"math/rand"
	"time"
)

const iterations = 1000
const sliceSize = 10
const min = 1
const max = 10

func isEven(x int) bool {
	return x%2 == 0
}

func sumEvens(v []int) int {
	n := len(v)
	sum := 0

	for i := 0; i < n; i++ {
		if isEven(v[i]) {
			sum += v[i]
		}
	}

	return sum
}

func generateRandomNumber(min int, max int) int {
	rand.Seed(time.Now().UnixNano())

	// rand.Intn returns a pseudo-random number in the half-open interval [0,n),
	// so (max + 1 - min) will return a number in the interval [min, max]
	return rand.Intn(max+1-min) + min
}

func generateRandomSlice(size int, min int, max int) []int {
	randomSlice := make([]int, size)
	for i := range randomSlice {
		randomSlice[i] = generateRandomNumber(min, max)
	}
	return randomSlice
}

func main() {
	evenSum := 0

	for i := 0; i < iterations; i++ {
		v := generateRandomSlice(sliceSize, min, max)
		evenSum += sumEvens(v)
	}

	avgEvenSum := float64(evenSum) / iterations

	fmt.Printf("A média da soma dos pares obtida em %d experimentos de vetores de tamanho %d "+
		"com números no intervalo [%d, %d] foi %.3f.",
		iterations,
		sliceSize,
		min,
		max,
		avgEvenSum,
	)
}
