package main

import (
	"fmt"
	"math"
)

func initializeMatrix(n int) [][]int {
	matrix := make([][]int, n)
	for i := range matrix {
		matrix[i] = make([]int, n)
	}

	return matrix
}

func minimalMatrixMultiplicationChainOperations(chain []int, n int) int {
	// Vetores em Go comecam em 0.
	// Para facilitar, ignoramos a coluna e linha 0
	matrix := initializeMatrix(n)

	// Não precisamos multiplicar uma matriz por ela mesma.
	// Custo é zero
	for i := 1; i < n; i++ {
		matrix[i][i] = 0
	}

	// L percorre a cadeia
	for L := 2; L < n; L++ {
		fmt.Printf("\n\n%d matrizes: : \n", L)
		for i := 1; i < n-L+1; i++ {
			j := i + L - 1

			if j == n {
				continue
			}

			matrix[i][j] = math.MaxInt

			fmt.Printf("m[%d][%d]: \n", i, j)
			for k := i; k < j; k++ {
				q := matrix[i][k] + matrix[k+1][j] + chain[i-1]*chain[k]*chain[j]
				fmt.Printf("    m[%d][%d] + m[%d][%d] + p[%d] * p[%d] * p[%d] = %d\n", i, k, k+1, j, i-1, k, j, q)
				if q < matrix[i][j] {
					fmt.Printf("    m[%d][%d] receives %d. Previously, it was %d\n\n", i, j, q, matrix[i][j])
					matrix[i][j] = q
				}
			}
		}
	}

	return matrix[1][n-1]
}

func main() {
	chain := []int{5, 10, 3, 12, 5, 50, 6}
	fmt.Printf("Min operations is: %d", minimalMatrixMultiplicationChainOperations(chain, 7))
}
