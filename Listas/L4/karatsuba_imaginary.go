package main

type Complex struct {
	Real      float64
	Imaginary float64
}

func multiplyComplex(first, second Complex) Complex {
	a := first.Real
	b := first.Imaginary
	c := second.Real
	d := second.Imaginary

	x := c * (a + b)
	y := a * (d - c)
	z := b * (c + d)

	return Complex{
		Real:      x - z,
		Imaginary: x + y,
	}
}

func main() {

}
