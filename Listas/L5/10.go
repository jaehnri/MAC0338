package main

import "fmt"

type Building struct {
	Left   int
	Right  int
	Height int
}

func NewBuilding(left int, height int, right int) Building {
	return Building{
		Left:   left,
		Right:  right,
		Height: height,
	}
}

// Skyline é uma estrutura auxiliar que guarda um ponto em que a Skyline
// muda de altura e essa altura correspondente. Na prática, uma
// Skyline vai ser representada por um vetor dessa estrutura abaixo.
type Skyline struct {
	Left   int
	Height int
}

func NewSkyline(left int, height int) Skyline {
	return Skyline{
		Left:   left,
		Height: height,
	}
}

func (b *Building) toSkyline() []Skyline {
	var result []Skyline

	return append(result,
		NewSkyline(b.Left, b.Height),
		NewSkyline(b.Right, 0))
}

// Essa funcão é útil pois se duas Skylines tiverem um prédio
// no mesmo X, queremos mostrar a de maior altura.
func max(leftHeight int, rightHeight int) int {
	if leftHeight > rightHeight {
		return leftHeight
	}

	return rightHeight
}

func mergeSkylines(leftSkyline []Skyline, rightSkyline []Skyline) []Skyline {
	leftCounter := 0
	rightCounter := 0
	leftHeight := 0
	rightHeight := 0
	var result []Skyline

	for leftCounter < len(leftSkyline) && rightCounter < len(rightSkyline) {

		// A Skyline da esquerda tem um "ponto crítico" antes do que a direita.
		// Um ponto crítico é onde uma skyline muda de altura. Isto é, o primeiro X
		// de um prédio, que é o que estamos chamando de "Skyline.Left".
		if leftSkyline[leftCounter].Left < rightSkyline[rightCounter].Left {
			leftHeight = leftSkyline[leftCounter].Height
			newSkyline := Skyline{
				Left:   leftSkyline[leftCounter].Left,
				Height: max(leftHeight, rightHeight),
			}

			result = append(result, newSkyline)
			leftCounter++
		} else {
			// Skyline da direita tem um ponto crítico anbtes do que a esquerda.
			rightHeight = rightSkyline[rightCounter].Height
			newSkyline := Skyline{
				Left:   rightSkyline[rightCounter].Left,
				Height: max(leftHeight, rightHeight),
			}

			result = append(result, newSkyline)
			rightCounter++
		}
	}

	// Uma das skylines acabou, então adicionamos o resto da outra.
	for ; leftCounter < len(leftSkyline); leftCounter++ {
		result = append(result, leftSkyline[leftCounter])
	}
	for ; rightCounter < len(rightSkyline); rightCounter++ {
		result = append(result, rightSkyline[rightCounter])
	}

	return result
}

func createSkyline(buildings []Building) []Skyline {
	if len(buildings) < 2 {
		return buildings[0].toSkyline()
	}

	left := createSkyline(buildings[:len(buildings)/2])
	right := createSkyline(buildings[len(buildings)/2:])
	return mergeSkylines(left, right)
}

func main() {
	var buildings []Building

	buildings = append(buildings,
		NewBuilding(15, 7, 20),
		NewBuilding(4, 8, 10),
		NewBuilding(18, 9, 21),
		NewBuilding(2, 4, 11),
		NewBuilding(7, 3, 17),
		NewBuilding(6, 6, 9),
		NewBuilding(14, 2, 22),
	)

	fmt.Print(createSkyline(buildings))
}
