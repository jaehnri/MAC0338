package main

func count(a []int, k int) []int {
	// Cria um vetor b de tamanho k + 1, que salva quantas vezes aparece
	// cada número. Em Go, os vetores começam em 0, mas ainda queremos acessar
	// a quantidade de números 2 fazendo b[2], por exemplo.
	b := make([]int, k+1)

	// Conta quantas vezes cada número de 1 a k
	// aparece no vetor a.
	for _, i := range a {
		b[i]++
	}

	return b
}

func createCumulativeSlice(b []int, k int) []int {
	c := make([]int, k+1)

	// Transforma b := [0 2 2 1 1 1] em c := [0 2 4 5 6 7]
	for i := range b {

		// Evitar c[i - 1] == c[-1]
		if i == 0 {
			continue
		}

		c[i] = c[i-1] + b[i]
	}

	return c
}

func getQuantityInInterval(c []int, a int, b int) int {
	return c[b] - c[a-1]
}
